# ~/.bashrc

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

HISTFILESIZE=10000
HISTSIZE=1000
# ignoreboth: ignore duplicate entries and commands beginning with a space
HISTCONTROL=ignoreboth
export HISTFILESIZE HISTSIZE HISTCONTROL

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"
if [ -r /usr/share/source-highlight/src-hilite-lesspipe.sh ]; then
        export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
        export LESS=' -R '
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
elif [ -f /usr/local/etc/bash_completion ]; then
    . /usr/local/etc/bash_completion
fi
if [ -r /usr/local/share/mercurial/contrib/bash_completion ]; then
   . /usr/local/share/mercurial/contrib/bash_completion
fi
if [ -r /usr/local/bin/aws_completer ]; then
   complete -C '/usr/local/bin/aws_completer' aws
fi

### Variables
# color grep output
export GREP_COLOR='00;38;5;136'
export GREP_OPTIONS='--color=auto'
export EDITOR=emacs
# GPG always wants to know what TTY it's running on.
export GPG_TTY=`tty`

# solarize
eval `dircolors ~/.dircolors.256dark`

# Set a fancy color prompt
# Show the current git branch in the prompt?
if [ -f /usr/lib/git-core/git-sh-prompt ]; then
	source /usr/lib/git-core/git-sh-prompt
	PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1 " (%s)")\$ '
elif [ -f /usr/local/share/git-core/contrib/completion/git-prompt.sh ]; then
	source /usr/local/share/git-core/contrib/completion/git-prompt.sh
	PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1 " (%s)")\$ '	
else
	PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
fi

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
    ;;
*)
    ;;
esac

if [ -n "$DISPLAY" -a "$TERM" == "xterm" ]; then
	export TERM=xterm-256color
fi

# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
# Source site specific file if it exists
if [ -e ~/.bashrc.site ]; then
    source ~/.bashrc.site
fi
# More specific aliases on this host?
if [ -f ~/.bash_aliases.`hostname -s` ]; then
    . ~/.bash_aliases.`hostname -s`
fi


pathadd() {
          if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
             PATH="$1:$PATH"
          fi
}
pathadd /sbin
pathadd ~/bin

