# Alias definitions.

platform='unknown'
unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
	platform='linux'
elif [[ "$unamestr" == 'FreeBSD' ]]; then
	platform='freebsd'
fi
if [[ $platform == 'linux' ]]; then
	alias ls='ls --color=auto'
    	alias dir='ls --color=auto --format=vertical'
    	alias vdir='ls --color=auto --format=long'
elif [[ $platform == 'freebsd' ]]; then
	alias ls='ls -G'
	alias top='top -P'
fi

alias ll='ls -lh'
alias llt='ls -lhtr'
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'
alias bc='bc -l'
alias df='df -h'
alias toplist='sort | uniq -c | sort -rn'
alias t='tmux att -d'

# nicer less
alias less='less --RAW-CONTROL-CHARS --quit-if-one-screen --no-init'
